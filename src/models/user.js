require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/user')


module.exports = (sequelize) => {

  class User extends Model {

    static associate(models) {
      models.User.belongsTo(models.Role, { as: 'role' });

      models.User.addScope('withRole', {
        include: [
          {
            model: models.Role, as: 'role'
          }
        ]
      });

      models.User.addScope('withAuthorities', {
        include: [
          {
            model: models.Role, as: 'role',
            include: [
              {
                model: models.Authority,
                as: 'authorities',
                attributes: {
                  exclude: []
                },
                through: {
                  attributes: []
                }
              }
            ]
          }
        ]
      });

    }

  };

  User.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        let now = new Date();
        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;

        if (options.user) {
          let user = options.user.id + ',' + options.user.username;
          instance.createdBy = user;
          instance.updatedBy = user;
        }
      },

      beforeUpdate: function beforeUpdate(instance, options) {
        instance.updatedAt = new Date();

        if (options.user) {
          instance.updatedBy = options.user.id + "," + options.user.username;
        }
      }

    }
  });

  return User;

};