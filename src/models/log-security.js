require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/log-security');


module.exports = (sequelize, DataTypes) => {

  class LogSecurity extends Model {

    static associate(models) {
      // 
    }

  };

  LogSecurity.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });

  return LogSecurity;
};