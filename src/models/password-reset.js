require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/password-reset');


module.exports = (sequelize, DataTypes) => {

  class PasswordReset extends Model {

    static associate(models) {
      // 
    }

  };

  PasswordReset.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });

  return PasswordReset;
};