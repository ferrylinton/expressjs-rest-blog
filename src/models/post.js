require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/post');


module.exports = (sequelize, DataTypes) => {

  class Post extends Model {

    static associate(models) {

      models.Post.belongsToMany(models.Tag, {
        through: 'blg_tag_post',
        foreignKey: 'post_id',
        as: 'tags',
        timestamps: false
      });

      models.Post.addScope('withTags', {
        include: [
          {
            model: models.Tag,
            as: 'tags',
            attributes: ['id', 'name'],
            through: {
              attributes: []
            }
          }
        ]
      });

    }

  };

  Post.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        let now = new Date();
        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;

        if (options.user) {
          let user = options.user.id + ',' + options.user.username;
          instance.createdBy = user;
          instance.updatedBy = user;
        }
      },

      beforeUpdate: function beforeUpdate(instance, options) {
        instance.updatedAt = new Date();

        if (options.user) {
          instance.updatedBy = options.user.id + "," + options.user.username;
        }
      }

    }
  });

  return Post;
};