require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/log-create');

module.exports = (sequelize, DataTypes) => {

  class LogCreate extends Model {

    static associate(models) {
      // 
    }

  };

  LogCreate.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }
      
    }
  });

  return LogCreate;
};