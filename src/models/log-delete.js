require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/log-delete');

module.exports = (sequelize, DataTypes) => {

  class LogDelete extends Model {

    static associate(models) {
      // 
    }

  };

  LogDelete.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });

  return LogDelete;
};