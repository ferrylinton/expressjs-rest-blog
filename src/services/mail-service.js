'use strict';

require('module-alias/register');
const nodemailer = require('nodemailer');
const templateService = require('@services/template-service');

const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    secureConnection: true,
    port: parseInt(process.env.EMAIL_PORT, 10),
    tls: {
        rejectUnauthorized: false
    },
    auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD
    }
});

async function sendConfirmation(user, userConfirmation) {
    let data = {
        username: user.username,
        url: process.env.BASE_URL + '/users/confirm/' + userConfirmation.id
    };

    let mailOptions = {
        from: process.env.EMAIL_FROM,
        to: user.email,
        subject: 'User Confirmation',
        text: templateService.getHtml('user-confirmation.hbs', data)
    };

    return await transporter.sendMail(mailOptions);
}

module.exports = {
    sendConfirmation
};
