require('module-alias/register');

const _ = require('lodash');
const { Sequelize, Op } = require('sequelize');
const { where, fn, col } = Sequelize;
const BadRequestError = require('@errors/bad-request-error');
const queryUtil = require('@utils/query-util');
const errorUtil = require('@utils/error-util');
const { formatDate } = require('@utils/sequelize');
const tagService = require('@services/tag-service');
const uploadService = require("@services/upload-service");
const { sequelize, Post, Tag } = require('@models/index');
const order = [['title', 'ASC']];
const exclude = ['content'];
const include = [[fn(formatDate(), col('updated_at'), '%d-%m-%Y'), 'updatedAtDate']];

async function findAll(req) {
  let params = queryUtil.getParams(Post, req, order);
  params.attributes = { exclude, include };

  return findAndCountAll(params, req);
}

async function findAllByTag(req) {
  let params = queryUtil.getParams(Post, req, order);
  params.attributes = { exclude, include };
  params.include = [
    {
      model: Tag,
      as: 'tags',
      attributes: [],
      through: {
        attributes: []
      }
    }
  ];

  // add Tag parameter
  let tagFilters = queryUtil.getFilters(params);
  if (tagFilters) {
    params.include[0].where = {
      [Op.and]: [where(fn('lower', col('name')), { [Op.eq]: req.params.tag.toLowerCase() }), { [Op.or]: tagFilters }]
    }
  } else {
    params.include[0].where = where(fn('lower', col('name')), { [Op.eq]: req.params.tag.toLowerCase() });
  }

  return findAndCountAll(params, req);
}

async function findAndCountAll(params, req) {
  let count = await Post.count(params);
  let pagination = queryUtil.getPagination(req, count, params);
  let rows = await Post.findAll(params);
  return { rows, count, pagination };
}

async function findRandom(req) {
  let params = {
    include: [
      {
        model: Tag,
        as: 'tags',
        attributes: [],
        through: {
          attributes: []
        }
      }
    ],
    attributes: ['code', 'title', 'updatedAt', 'step'],
    limit: 5,
    offset: 0
  };

  let randoms = [];

  if (req.query.tags) {
    let tags = req.query.tags.split(',');

    for (let i = 0; i < tags.length; i++) {
      if (i === 2) { break; }

      let random = { tag: tags[i], content: [] }

      // set filter by tag
      params.include[0].where = { name: tags[i] }

      // Get latest post
      params.order = [['updatedAt', 'DESC']];

      let data = {};
      data.posts = await Post.findAll(params);
      if (data.posts.length > 0) {
        data.label = 'latest';
        random.content.push(data);
      }

      // Get posts and order by step
      params.order = [['step', 'ASC']]

      data = {};
      data.posts = await Post.findAll(params);
      if (data.posts.length > 0) {
        data.label = 'sequence';
        random.content.push(data);
      }

      if(random.content.length > 0){
        randoms.push(random);
      }
      

    };
  }

  return randoms;
}

async function findOne(obj) {
  const id = _.isString(obj) ? obj : obj.params.id;
  const post = await Post.findOne({
    where: { id }
  });

  if (post) {
    return post;
  } else {
    errorUtil.throwNotFoundError(obj);
  }
}

async function findOneWithTags(id) {
  const post = await Post.scope('withTags').findOne({
    where: { id }
  });

  if (post) {
    return post;
  } else {
    errorUtil.throwNotFoundError(id);
  }
}

async function findById(obj) {
  const id = _.isString(obj) ? obj : obj.params.id;
  const post = await Post.findOne({
    attributes: { exclude: ['content'] },
    where: { id }
  });

  if (post) {
    return post;
  } else {
    errorUtil.throwNotFoundError(obj);
  }
}

async function findByCode(obj) {
  const code = _.isString(obj) ? obj : obj.params.code;
  const post = await Post.findOne({
    attributes: [
      'id', 'code', 'title', 'tagNames', 'updatedAt', 'createdBy', 'content',
      [fn(formatDate(), col('updated_at'), '%d-%m-%Y'), 'updatedAtDate']
    ],
    where: { code }
  });

  if (post) {
    return post;
  } else {
    errorUtil.throwNotFoundError(obj);
  }
}

async function create(req) {
  let { code, title, description, step } = req.body;
  let { tags, tagNames } = await tagService.getTags(req.body.tags);

  if (tags.length == 0) {
    throw new BadRequestError(`[${req.body.tags.join()}] is not valid`);
  }

  let post = await sequelize.transaction(async (t) => {
    let options = { user: req.user, transaction: t };
    let content = uploadService.readFile(req);
    let post = await Post.create({ code, title, description, tagNames, step, content }, options);

    await post.setTags(tags, options);
    return post.toJSON();
  });

  return await findById(post.id);
}

async function update(req) {
  let post = await findOneWithTags(req.params.id);
  let tags = post.tags;
  let changes = 0;


  if (req.body.tags) {
    let result = await tagService.getTags(req.body.tags);
    post.tags = result.tags;
    post.tagNames = result.tagNames;

    if (post.tags.length == 0) {
      throw new BadRequestError(`[${req.body.tags.join()}] is not valid`);
    }

    changes++;
  }

  if (req.body.code) {
    post.code = req.body.code;
    changes++;
  }

  if (req.body.title) {
    post.title = req.body.title;
    changes++;
  }

  if (req.body.description) {
    post.description = req.body.description;
    changes++;
  }

  if (req.body.step) {
    post.step = req.body.step;
    changes++;
  }

  if (req.file) {
    post.content = uploadService.readFile(req);
    changes++;
  }

  if (changes > 0) {
    post = await sequelize.transaction(async (t) => {
      let options = { user: req.user, transaction: t };
      await post.save(options);
      await post.setTags(tags, options);

      return post.toJSON();
    });

    post.tags = tags;
  } else {
    post = post.toJSON();
  }

  delete post.content;
  return post;
}

async function destroy(req) {
  return await sequelize.transaction(async (t) => {
    let post = await findOne(req, t);
    await post.setTags([], { user: req.user, transaction: t });
    await post.destroy({ transaction: t });

    return post.toJSON();
  });
}

module.exports = {
  findAll,
  findAllByTag,
  findRandom,
  findById,
  findByCode,
  create,
  update,
  destroy
};
