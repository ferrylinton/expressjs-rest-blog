require('module-alias/register');

const { uid } = require('uid');
const requestIp = require('request-ip');
const { sequelize, User, UserConfirmation } = require('@models');
const logger = require('@configs/logger');
const mailService = require('@services/mail-service');
const templateService = require('@services/template-service');

async function confirm(req, res, next) {
  let data = {
    status: false,
    msg: 'Tautan tidak valid'
  };

  const userConfirmation = await UserConfirmation.findOne({
    where: { id: req.params.id }
  });

  if (userConfirmation && !userConfirmation.status && isDateValid(userConfirmation.requestDate)) {
    let result = updateStatus(userConfirmation.id, userConfirmation.userId, 'SUCCESS');

    if (result) {
      data.status = true;
      data.msg = 'Pengguna dikonfirmasi';
    }
  }

  res.status(200)
  res.set('Content-Type', 'text/html');
  res.send(templateService.getHtml('confirm.hbs', data));
}

async function resend(req, res, next) {
  let data = {
    status: false,
    msg: 'Gagal mengirim email'
  };

  const user = await User.findOne({
    where: { id: req.params.id }
  });

  if (user) {

    if (user.activated) {
      data.msg = 'Pengguna sudah diaktivasi';
    } else {
      let userConfirmation = {
        id: uid(),
        ip: requestIp.getClientIp(req),
        requestDate: new Date(),
        userId: user.id
      };

      userConfirmation = await UserConfirmation.create(userConfirmation);
      let info = await mailService.sendConfirmation(user, userConfirmation);

      if (info.accepted && info.accepted.includes(user.email)) {
        data.status = true;
        data.msg = 'Email terkirim';
      }
    }
  }

  res.status(200)
  res.set('Content-Type', 'text/html');
  res.send(templateService.getHtml('resend.hbs', data));
}

async function create(req, user, transaction) {
  let userConfirmation = {
    id: uid(),
    ip: requestIp.getClientIp(req),
    requestDate: new Date(),
    userId: user.id
  };

  userConfirmation = await UserConfirmation.create(userConfirmation, { transaction });
  let mail = await mailService.sendConfirmation(user, userConfirmation);
  return { mail, userConfirmation };
}

async function updateStatus(id, userId, status) {
  try {
    await sequelize.transaction(async (t) => {

      await User.update(
        { activated: true },
        { where: { id: userId }, transaction: t }
      );

      await UserConfirmation.update(
        { status },
        { where: { id }, transaction: t }
      );

    });

    return true;
  } catch (err) {
    logger.error(err.stack);
    return false;
  }
}

function isDateValid(requestDate) {
  var now = new Date();
  now.setMinutes(now.getSeconds() - (15 * 60 + 10));
  return requestDate > now
}

module.exports = {
  confirm,
  create,
  resend
};
