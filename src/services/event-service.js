require('module-alias/register');

const requestIp = require('request-ip');
const { EventEmitter } = require('events');
const eventEmitter = new EventEmitter();

function init(req) {
    let data = {
        ip: requestIp.getClientIp(req),
        userAgent: req.headers['user-agent'],
        host: req.headers.host,
        path: (req.baseUrl === '/' ? '' : req.baseUrl) + (req.path === '/' ? '' : req.path),
        method: req.method
    }

    if (req.user) {
        data.username = req.user.username;
    }

    return data;
}

module.exports = {
    eventEmitter,
    init
}
