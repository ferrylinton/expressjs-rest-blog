require('module-alias/register');

const _ = require('lodash');
const { Op } = require('sequelize');
const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const { Authority } = require('@models/index');
const order = [['name', 'ASC']];


async function getAll() {
  return await Authority.findAll({ attributes: ['id', 'name'], order });
}

async function findAndCountAll(req) {
  let params = queryUtil.getParams(Authority, req, order);
  let count = await Authority.count(params);
  let pagination = queryUtil.getPagination(req, count, params);
  let rows = await Authority.findAll(params);
  return { rows, count, pagination };
}

async function findOne(req) {
  const authority = await Authority.findOne({
    where: { id: req.params.id }
  });

  if (authority) {
    return authority;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function create(req) {
  let { name } = req.body;
  return await Authority.create({ name }, { user: req.user });
}

async function update(req) {
  let authority = await findOne(req);
  authority.name = req.body.name;

  return await authority.save({ user: req.user });
}

async function destroy(req) {
  let authority = await findOne(req);
  await authority.destroy({ user: req.user });
  return authority.toJSON();
}

async function getAuthorities(names) {
  if (names) {
    return await Authority.findAll({
      attributes: ['id', 'name'],
      where: {
        name: {
          [Op.in]: names
        }
      }
    });
  } else {
    return [];
  }
}

module.exports = {
  getAll,
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  getAuthorities
};
