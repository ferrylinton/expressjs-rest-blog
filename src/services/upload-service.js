require('module-alias/register');

const path = require('path');
const fs = require('fs');
const multer = require("multer");
const logger = require("@configs/logger");

const folder = path.join(process.cwd(), 'uploads');
const upload = multer({ dest: folder });

function clear(req) {
    if (req && req.file) {
        try {
            let filePath = getFilePath(req);
            fs.unlinkSync(filePath);
            delete req['file'];
            logger.info(`${filePath} is deleted`)
        } catch (err) {
            logger.error(err.stack);
        }
    }
}

function readFile(req) {
    return fs.readFileSync(getFilePath(req));
}

function getFilePath(req) {
    return path.join(folder, req.file.filename);
}

function single() {
    return upload.single('file');
}


module.exports = {
    upload,
    single,
    clear,
    readFile
}