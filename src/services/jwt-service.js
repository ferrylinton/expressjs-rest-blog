require('module-alias/register');

const path = require('path');
const fs = require('fs');
const lodash = require('lodash');
const { uid } = require('uid');
const jsonwebtoken = require('jsonwebtoken');

function createFromUser(user) {
    let payload = {
        id: user.id,
        username: user.username,
        permissions: user.role.authorities.map(authority => authority.name)
    }

    return createToken(payload);
}

function createFromToken(decoded) {
    let payload = {
        id: decoded.jti,
        username: decoded.username,
        permissions: decoded.permissions
    }

    return createToken(payload);
}

function createToken(payload) {
    let exp = process.env.JWT_EXPIRES_IN;
    let token = jsonwebtoken.sign(
        payload,
        fs.readFileSync(path.join(process.cwd(), 'security-keys', 'private.key'), 'utf8'),
        {
            algorithm: 'RS256',
            expiresIn: lodash.isInteger(exp) ? parseInt(exp) : exp,
            issuer: process.env.JWT_ISSUER,
            jwtid: uid(32)
        }
    );

    return { token }
}

module.exports = {
    createFromUser,
    createFromToken
}