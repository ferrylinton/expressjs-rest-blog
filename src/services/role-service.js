require('module-alias/register');

const NotFoundError = require('@errors/not-found-error');
const BadRequestError = require('@errors/bad-request-error');
const queryUtil = require('@utils/query-util');
const authorityService = require('@services/authority-service');
const { sequelize, Role } = require('@models/index');
const order = [['name', 'ASC']];

async function getAll() {
  return await Role.findAll({ attributes: ['id', 'name'], order });
}

async function findAndCountAll(req) {
  let params = queryUtil.getParams(Role, req, order);
  let count = await Role.count(params);
  let pagination = queryUtil.getPagination(req, count, params);
  let rows = await Role.findAll(params);
  return { rows, count, pagination };
}
async function findOne(req) {
  let role = await Role.scope('withAuthorities').findOne({
    where: { id: req.params.id }
  });

  if (role) {
    return role;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function create(req) {
  let { name } = req.body;
  let authorities = await authorityService.getAuthorities(req.body.authorities);

  let role = await sequelize.transaction(async (t) => {
    let options = { user: req.user, transaction: t };
    let role = await Role.create({ name }, options);
    await role.setAuthorities(authorities, options);
    return role.toJSON();
  });

  role.authorities = authorities;
  return role;
}

async function update(req) {
  let role = await findOne(req);
  let authorities = [];
  let changes = 0;

  if (req.body.hasOwnProperty('authorities')) {
    authorities = await authorityService.getAuthorities(req.body.authorities);

    if (authorities.length == 0) {
      throw new BadRequestError(`[${req.body.authorities.join()}] is not valid`);
    }

    changes++;
  }

  if (req.body.hasOwnProperty('name')) {
    role.name = req.body.name;
    changes++;
  }

  if (changes > 0) {
    role = await sequelize.transaction(async (t) => {
      let options = { user: req.user, transaction: t };
      await role.save(options);
      await role.setAuthorities(authorities, options);

      return role.toJSON();
    });

    role.authorities = authorities;
  }

  return role;
}

async function destroy(req) {
  return await sequelize.transaction(async (t) => {
    let role = await findOne(req);
    await role.setAuthorities([], { user: req.user, transaction: t });
    await role.destroy({ transaction: t });
    return role.toJSON();
  });
}

async function findByName(name) {
  let role = await Role.findOne({
    where: { name }
  });

  if (role) {
    return role
  } else {
    role = await Role.findOne({
      where: { name: 'User' }
    });

    return role
  }
}

module.exports = {
  getAll,
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  findByName
};

