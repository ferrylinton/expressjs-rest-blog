require('module-alias/register');

const lodash = require('lodash');
const { Op } = require('sequelize');
const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const { Tag } = require('@models/index');
const order = [['name', 'ASC']];

async function getAll() {
  return await Tag.findAll({ attributes: ['id', 'name'], order });
}

async function findAndCountAll(req) {
  let params = queryUtil.getParams(Tag, req, order);
  let count = await Tag.count(params);
  let pagination = queryUtil.getPagination(req, count, params);
  let rows = await Tag.findAll(params);
  return { rows, count, pagination };
}

async function findOne(req) {
  const tag = await Tag.findOne({
    where: { id: req.params.id }
  });

  if (tag) {
    return tag;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function create(req) {
  let name = req.body.name.toLowerCase();
  return await Tag.create({ name }, { user: req.user });
}

async function update(req) {
  let tag = await findOne(req);
  tag.name = req.body.name.toLowerCase();
  return await tag.save({ user: req.user });
}

async function destroy(req) {
  let tag = await findOne(req);
  await tag.destroy({ user: req.user });
  return tag.toJSON();
}

async function getTags(names) {
  let tags = [];
  let tagNames = '';

  if (!lodash.isEmpty(names) && !lodash.isArray(names)) {
    names = [names];
  }

  if (names) {
    tags = await Tag.findAll({
      where: {
        name: {
          [Op.in]: names
        }
      }
    });

    tagNames = tags.map(function (tag) {
      return tag.name
    });

    tagNames = tagNames.toString()
  }

  return { tags, tagNames };
}


module.exports = {
  getAll,
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  getTags
};
