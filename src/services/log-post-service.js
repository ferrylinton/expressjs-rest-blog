require('module-alias/register');

const lodash = require('lodash');
const { QueryTypes } = require('sequelize');
const logger = require('@configs/logger');
const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const securityService = require('@services/security-service');
const { eventEmitter, init } = require('@services/event-service');
const { sequelize, LogPost } = require('@models/index');
const EVENT = 'LOG_POST_EVENT';
const attributes = ['id', 'ip', 'userAgent', 'host', 'path', 'requestDate'];
const order = [['requestDate', 'DESC']];

eventEmitter.on(EVENT, listener);

async function listener(req) {
    try {
        let data = init(req);
        let {params, query} = req;
        data.params = JSON.stringify({...params, ...query});
        await LogPost.create(data);
    } catch (err) {
        logger.error(err.stack);
    }
}

function log(req, res, next) {
    try {
        if (!req.user) {
            req.user = securityService.getUserFromToken(req);
        }

        eventEmitter.emit(EVENT, req);
    } catch (err) {
        logger.error(err.stack);
    }

    next();
}

async function findAndCountAll(req) {
    let params = queryUtil.getParams(LogPost, req, order);
    params.attributes = attributes;

    let count = await LogPost.count(params);
    let pagination = queryUtil.getPagination(req, count, params);
    let rows = await LogPost.findAll(params);
    return { rows, count, pagination };
}

async function findOne(req) {
    const logPost = await LogPost.findOne({
        where: { id: req.params.id }
    });

    if (logPost) {
        return logPost;
    } else {
        throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
    }
}

async function destroy(req) {
    if (req.query.number) {
        let sql = `select id from log_post order by request_datetime ASC LIMIT ?`;
        let options = {
            replacements: [lodash.toInteger(req.query.number)],
            type: QueryTypes.SELECT
        };
        let ids = await sequelize.query(sql, options);
        ids = ids.map(obj => {
            return obj.id
        })

        sql = `delete from log_post where id in (?)`;
        options = {
            replacements: [ids],
            type: QueryTypes.DELETE
        };

        await sequelize.query(sql, options);
    }
}

module.exports = {
    log,
    findAndCountAll,
    findOne,
    destroy
}
