require('module-alias/register');

const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const uploadService = require('@services/upload-service');
const { Image } = require('@models/index');
const order = [['name', 'ASC']];


async function findAndCountAll(req) {
  let params = queryUtil.getParams(Image, req, order);
  let count = await Image.count(params);
  let pagination = queryUtil.getPagination(req, count, params);
  let rows = await Image.scope('noimage').findAll(params);
  return { rows, count, pagination };
}

async function findOne(req) {
  const image = await Image.scope('noimage').findOne({
    where: { id: req.params.id }
  });

  if (image) {
    return image;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function view(req) {
  let image = await Image.findOne({
    where: {
      name: req.params.name
    }
  });

  if (image) {
    return image;
  } else {
    throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
  }
}

async function create(req) {
  let name = req.body.name;
  let contentType = req.file.mimetype;
  let bytes = uploadService.readFile(req);

  let image = await Image.create({ name, contentType, bytes }, { user: req.user });
  uploadService.clear(req);
  return toJSON({ image, req });
}

async function update(req) {
  let image = await findOne(req);
  let changes = 0;

  if (req.body.name) {
    image.name = req.body.name;
    changes++;
  }

  if (req.file) {
    image.contentType = req.file.mimetype;
    image.bytes = uploadService.readFile(req);
    changes++;
  }

  if (changes > 0) {
    image = await image.save({ user: req.user });
    uploadService.clear(req);
  }

  return toJSON({ image, req });
}

async function destroy(req) {
  let image = await findOne(req);
  await image.destroy({ user: req.user });
  return toJSON({ image, req });
}

function toJSON({ image, req }) {
  image = image.toJSON();
  delete image['bytes'];
  return image;
}

module.exports = {
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  view
};
