require('module-alias/register');

const lodash = require('lodash');
const { QueryTypes } = require('sequelize');
const logger = require('@configs/logger');
const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const { eventEmitter, init } = require('@services/event-service');
const { sequelize, LogSecurity } = require('@models/index');
const EVENT = 'LOG_SECURITY';
const attributes = ['id', 'ip', 'userAgent', 'host', 'path', 'requestDate'];
const order = [['requestDate', 'DESC']];

eventEmitter.on(EVENT, listener);

function listener({ req, statusCode }) {
    try {
        let data = init(req);
        data.username = getUsername(req);
        data.statusCode = statusCode;

        LogSecurity
            .create(data)
            .then(logSecurity => {
                logger.info(JSON.stringify(logSecurity.toJSON()));
            }).catch((err) => {
                logger.error(err.stack);
            })

    } catch (err) {
        logger.error(err.stack);
    }
}

function getUsername(req) {
    if (req.user) {
        return req.user.username
    } else if (req.body) {
        return req.body.username;
    }

    return nulll;
}

function log(req, res, next) {
    res.on('finish', () => {
        try {
            let statusCode = res.statusCode;
            eventEmitter.emit(EVENT, { req, statusCode });
        } catch (err) {
            logger.error(err.stack);
        }
    });

    next();
}

async function findAndCountAll(req) {
    let params = queryUtil.getParams(LogSecurity, req, order);
    params.attributes = attributes;

    let count = await LogSecurity.count(params);
    let pagination = queryUtil.getPagination(req, count, params);
    let rows = await LogSecurity.findAll(params);
    return { rows, count, pagination };
}

async function findOne(req) {
    const logSecurity = await LogSecurity.findOne({
        where: { id: req.params.id }
    });

    if (logSecurity) {
        return logSecurity;
    } else {
        throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
    }
}

async function destroy(req) {
    if (req.query.number) {
        let sql = 'delete from log_security ORDER BY request_datetime ASC LIMIT ?';
        let options = {
            replacements: [lodash.toInteger(req.query.number)],
            type: QueryTypes.DELETE
        };

        await sequelize.query(sql, options);
    }
}

module.exports = {
    log,
    findAndCountAll,
    findOne,
    destroy
}
