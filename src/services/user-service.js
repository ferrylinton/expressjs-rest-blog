require('module-alias/register');

const bcrypt = require('bcryptjs');
const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const userConfirmationService = require('@services/user-confirmation-service');
const roleService = require('@services/role-service');
const { User, Role, Authority, sequelize } = require('@models');
const order = [['username', 'ASC']];


async function findAndCountAll(req) {
  let params = queryUtil.getParams(User, req, order);
  let count = await User.count(params);
  let pagination = queryUtil.getPagination(req, count, params);
  let rows = await User.findAll(params);
  return { rows, count, pagination };
}

async function findOne(req) {
  const user = await User.scope('withRole').findOne({
    where: { id: req.params.id }
  });

  if (user) {
    return user;
  } else {
    var url = req.protocol + '://' + req.headers.host + req.originalUrl;
    throw new NotFoundError(`['${url}'] is not found`);
  }
}

async function create(req) {
  let role = await roleService.findByName(req.body.role);

  let username = req.body.username;
  let email = req.body.email;
  let roleId = role.id;
  let passwordHash = bcrypt.hashSync(req.body.password, 10);

  let user = await User.create({ username, email, passwordHash, roleId }, { user: req.user });
  return toJSON({ user, role, req });
}

async function update(req) {
  let user = await findOne(req);
  let role = user.role;
  let changes = 0;

  if (req.body.hasOwnProperty('role')) {
    role = await roleService.findByName(req.body.role);
    user.roleId = role.id;
    changes++;
  }

  if (req.body.hasOwnProperty('username')) {
    user.username = req.body.username;
    changes++;
  }

  if (req.body.hasOwnProperty('email')) {
    user.email = req.body.email;
    changes++;
  }

  if (req.body.hasOwnProperty('password')) {
    user.password = bcrypt.hashSync(req.body.password, 10);
    changes++;
  }

  if (req.body.hasOwnProperty('activated')) {
    user.activated = req.body.activated;
    changes++;
  }

  if (req.body.hasOwnProperty('locked')) {
    user.locked = req.body.locked;
    user.loginAttemptCount = 0;
    changes++;
  }

  if (changes > 0) {
    user = await user.save({ user: req.user });
  }

  return toJSON({ user, role, req });
}

async function destroy(req) {
  let user = await findOne(req);
  await user.destroy();
  return toJSON({ user, req });
}

async function findByUsername(username) {
  return await User.findOne({
    include: {
      model: Role,
      as: 'role',
      include: {
        model: Authority,
        as: 'authorities',
        attributes: {
          exclude: []
        },
        through: {
          attributes: []
        }
      }
    },
    where: { username }
  });
}

async function lock(username) {
  return await User.update(
    { locked: true },
    { where: { username } }
  );
}

async function activate(username) {
  return await User.update(
    { activated: true },
    { where: { username } }
  );
}

async function addLoginAttempt(user) {
  if (user.loginAttemptCount < 3) {
    user.loginAttemptCount = user.loginAttemptCount + 1;
  }

  if (user.loginAttemptCount == 3) {
    user.locked = true;
  }

  await user.save({ user });
}

async function resetLoginAttempt(user) {
  user.loginAttemptCount = 0;
  await user.save({ user });
}

async function register(req) {
  try {
    let { username, email } = req.body;
    let passwordHash = bcrypt.hashSync(req.body.password, 10);
    let role = await Role.findOne({
      where: { name: 'User' }
    });

    let user = { username, email, passwordHash }
    user.roleId = role.id;

    let result = await sequelize.transaction(async (t) => {
      let options = { transaction: t };
      user = await User.create(user, options);
      let { mail, userConfirmation } = await userConfirmationService.create(req, user, t);
      return { user, mail, userConfirmation };
    });


    return result;
  } catch (err) {
    throw err;
  }
}

function toJSON({ user, role, req }) {
  user = user.toJSON();
  hideAttributes({ user, req });

  if (role) {
    user.role = role;
  }

  return user;
}

function hideAttributes({ user, req }) {
  delete req.body['password'];
  delete req.body['passwordConfirmation'];
  delete user['passwordHash'];
  delete user['roleId'];
}

module.exports = {
  findAndCountAll,
  findOne,
  create,
  update,
  destroy,
  findByUsername,
  lock,
  activate,
  addLoginAttempt,
  resetLoginAttempt,
  register
};
