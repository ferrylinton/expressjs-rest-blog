require('module-alias/register');

const bcrypt = require('bcryptjs');
const jsonwebtoken = require('jsonwebtoken');
const AuthenticationError = require('@errors/authentication-error');
const NotFoundError = require('@errors/not-found-error');
const { RevokedToken } = require('@models/index');
const userService = require("@services/user-service");
const jwtService = require("@services/jwt-service");

async function authenticate(req) {
    let user = await userService.findByUsername(req.body.username)

    if (user) {
        if (!user.activated) {
            throw new AuthenticationError('User is not activated');
        }

        if (user.locked) {
            throw new AuthenticationError('User is locked');
        }

        if (bcrypt.compareSync(req.body.password, user.passwordHash)) {
            userService.resetLoginAttempt(user);
            return jwtService.createFromUser(user);
        } else {
            userService.addLoginAttempt(user);
        }
    }

    throw new AuthenticationError('Invalid username or password');
}

async function changePassword(req) {
    let user = await userService.findByUsername(req.user.username);

    if (user) {
        if (bcrypt.compareSync(req.body.oldPassword, user.passwordHash)) {
            user.passwordHash = bcrypt.hashSync(req.body.newPassword, 10);
            await user.save({ user: req.user });
        } else {
            throw new AuthenticationError('Invalid old password');
        }
    } else {
        throw new NotFoundError(`${req.user.username} is not found`);
    }
}

async function refresh(req) {
    let token = fromHeaderOrQueryString(req);

    if (token) {
        let decoded = jsonwebtoken.decode(token);
        return jwtService.createFromToken(decoded);
    }

    throw new AuthenticationError('Invalid token');
}

async function revoke(req) {
    let token = fromHeaderOrQueryString(req);

    if (token) {
        var decoded = jsonwebtoken.decode(token);
        return await RevokedToken.create({
            id: decoded.jti,
            token: token,
            username: req.user.username
        });
    }

    throw new AuthenticationError('Invalid token');
}

function getUserFromToken(req) {
    let token = fromHeaderOrQueryString(req);
    if (token) {
        var { username, permissions } = jsonwebtoken.decode(token);
        return { username, permissions };
    }

    return {};
}

async function register(req) {
    return await userService.register(req);
}

function fromHeaderOrQueryString(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
        return req.headers.authorization.split(' ')[1];
    else if (req.query && req.query.token)
        return req.query.token;

    return null;
}

module.exports = {
    authenticate,
    changePassword,
    revoke,
    refresh,
    register,
    getUserFromToken
};