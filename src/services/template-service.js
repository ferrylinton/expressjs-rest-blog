require('module-alias/register');

const fs = require('fs');
const path = require('path');
const Handlebars = require('handlebars');

function getHtml(filename, data) {
    let source = fs.readFileSync(path.join(process.cwd(), 'src', 'templates', filename), 'utf8');
    let template = Handlebars.compile(source);
    return template(data);
}

module.exports = {
    getHtml
};