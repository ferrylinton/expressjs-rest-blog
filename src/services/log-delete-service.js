require('module-alias/register');

const lodash = require("lodash");
const { QueryTypes } = require('sequelize');
const logger = require("@configs/logger");
const NotFoundError = require('@errors/not-found-error');
const queryUtil = require('@utils/query-util');
const { eventEmitter, init } = require("@services/event-service");
const { sequelize, LogDelete } = require('@models/index');
const EVENT = 'LOG_DELETE_EVENT';
const attributes = ['id', 'ip', 'userAgent', 'host', 'path', 'requestDate'];
const order = [['requestDate', 'DESC']];

eventEmitter.on(EVENT, listener);

async function listener({ req, statusCode, responseBody }) {
    try {
        let data = init(req);
        data.statusCode = statusCode;
        data.requestBody = JSON.stringify(req.body);
        data.responseBody = responseBody;
        data.params = JSON.stringify(req.params);
        await LogDelete.create(data);
    } catch (err) {
        logger.error(err.stack);
    }
}

function log(req, res, next) {
    let responseBody;
    let send = res.send;

    res.send = function (data) {
        responseBody = data;
        send.apply(res, arguments);
    }

    res.on('finish', () => {
        try {
            let statusCode = res.statusCode;
            eventEmitter.emit(EVENT, { req, statusCode, responseBody });
        } catch (err) {
            logger.error(err.stack);
        }
    });

    next();
}

async function findAndCountAll(req) {
    let params = queryUtil.getParams(LogDelete, req, order);
    params.attributes = attributes;

    let count = await LogDelete.count(params);
    let pagination = queryUtil.getPagination(req, count, params);
    let rows = await LogDelete.findAll(params);
    return { rows, count, pagination };
}

async function findOne(req) {
    const logDelete = await LogDelete.findOne({
        where: { id: req.params.id }
    });

    if (logDelete) {
        return logDelete;
    } else {
        throw new NotFoundError(`${req.protocol}://${req.headers.host}${req.originalUrl} is not found`);
    }
}

async function destroy(req) {
    if (req.query.number) {
        let sql = `select id from log_delete order by request_datetime ASC LIMIT ?`;
        let options = {
            replacements: [lodash.toInteger(req.query.number)],
            type: QueryTypes.SELECT
        };
        let ids = await sequelize.query(sql, options);
        ids = ids.map(obj => {
            return obj.id
        })

        sql = `delete from log_delete where id in (?)`;
        options = {
            replacements: [ids],
            type: QueryTypes.DELETE
        };

        await sequelize.query(sql, options);
    }
}

module.exports = {
    log,
    findAndCountAll,
    findOne,
    destroy
}
