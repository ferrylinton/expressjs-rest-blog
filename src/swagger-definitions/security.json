{
  "openapi": "3.0.3",
  "info": {
    "version": "0.0.1",
    "title": "Bologhu REST API",
    "description": "This is a REST API application for my blog. You can see my blog at [https://bologhu.com](https://bologhu.com)",
    "license": {
      "name": "MIT",
      "url": "https://opensource.org/licenses/MIT"
    },
    "contact": {
      "name": "Ferry L.H.",
      "email": "ferrylinton@gmail.com"
    }
  },
  "externalDocs": {
    "description": "Check My Website",
    "url": "https://bologhu.com"
  },
  "servers": [
    {
      "url": "https://api.bologhu.com",
      "description": "Production server"
    },
    {
      "url": "http://localhost:3001",
      "description": "Local server"
    }
  ],
  "security": [
    {
      "bearerAuth": []
    }
  ],
  "paths": {
    "/api/authenticate": {
      "post": {
        "operationId": "GetToken",
        "summary": "Login and retrieve an JWT Token",
        "security": [],
        "tags": [
          "Security"
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "email",
                  "password"
                ],
                "properties": {
                  "username": {
                    "type": "string",
                    "example": "user01",
                    "description": "Username"
                  },
                  "password": {
                    "type": "string",
                    "example": "password",
                    "description": "Password"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Token"
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/BadRequest"
          },
          "401": {
            "descrption": "Unauthorized",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "msg": {
                      "type": "string",
                      "example": "Invalid username or password"
                    }
                  }
                }
              }
            }
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    },
    "/api/refresh": {
      "get": {
        "operationId": "RefreshToken",
        "summary": "Refresh JWT Token",
        "tags": [
          "Security"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Token"
                }
              }
            }
          },
          "401": {
            "$ref": "#/components/responses/Unauthorized"
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    },
    "/api/revoke": {
      "get": {
        "operationId": "RevokeToken",
        "summary": "Revoke JWT Token",
        "tags": [
          "Security"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/RevokedToken"
                }
              }
            }
          },
          "401": {
            "$ref": "#/components/responses/Unauthorized"
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    },
    "/api/password": {
      "post": {
        "operationId": "ChangePassword",
        "summary": "Change Password",
        "tags": [
          "Security"
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "oldPassword",
                  "newPassword",
                  "newPasswordConfirmation"
                ],
                "properties": {
                  "oldPassword": {
                    "type": "string",
                    "example": "password",
                    "description": "User's Current Password"
                  },
                  "newPassword": {
                    "type": "string",
                    "example": "11111",
                    "description": "New Password"
                  },
                  "newPasswordConfirmation": {
                    "type": "string",
                    "example": "11111",
                    "description": "New Password Confirmation"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Token"
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/BadRequest"
          },
          "401": {
            "$ref": "#/components/responses/Unauthorized"
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    },
    "/api/register": {
      "post": {
        "operationId": "Register",
        "summary": "Register new User",
        "security": [],
        "tags": [
          "Security"
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "email",
                  "username",
                  "password",
                  "passwordConfirmation"
                ],
                "properties": {
                  "email": {
                    "type": "string",
                    "example": "ferry01@yopmail.com",
                    "description": "User's email"
                  },
                  "username": {
                    "type": "string",
                    "example": "ferry01",
                    "description": "User's username"
                  },
                  "password": {
                    "type": "string",
                    "example": "11111",
                    "description": "User's Password"
                  },
                  "passwordConfirmation": {
                    "type": "string",
                    "example": "11111",
                    "description": "User's Password Confirmation"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "OK",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "user": {
                      "$ref": "#/components/schemas/User"
                    },
                    "mail": {
                      "$ref": "#/components/schemas/Mail"
                    },
                    "userConfirmation": {
                      "$ref": "#/components/schemas/UserConfirmation"
                    }
                  }
                }
              }
            }
          },
          "400": {
            "$ref": "#/components/responses/BadRequest"
          },
          "401": {
            "descrption": "Unauthorized",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "msg": {
                      "type": "string",
                      "example": "Invalid username or password"
                    }
                  }
                }
              }
            }
          },
          "500": {
            "$ref": "#/components/responses/InternalServerError"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Mail": {
        "type": "object",
        "properties": {
          "accepted": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "example": [
              "ferry01@yopmail.com"
            ]
          },
          "rejected": {
            "type": "array",
            "items": {
              "type": "string"
            },
            "example": [
              "ferry01@yopmail.com"
            ]
          },
          "envelopeTime": {
            "type": "integer",
            "example": 86
          },
          "messageTime": {
            "type": "integer",
            "example": 514
          },
          "messageSize": {
            "type": "integer",
            "example": 1293
          },
          "response": {
            "type": "string",
            "example": "250 2.0.0 Ok: queued as 62DFE26C7D4"
          },
          "envelope": {
            "type": "object",
            "properties": {
              "from": {
                "type": "string",
                "example": "noreply@bologhu.com"
              },
              "to": {
                "type": "array",
                "items": {
                  "type": "string"
                },
                "example": [
                  "ferry01@yopmail.com"
                ]
              }
            }
          },
          "messageId": {
            "type": "string"
          }
        }
      },
      "RevokedToken": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "example": "d95f21c33734d28c71ca828a2c6ff567"
          },
          "token": {
            "type": "string",
            "example": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMGQyMmMwNDUxIiwidXNlcm5hbWUiOiJhZG1pbjAxIiwicGVybWlzc2lvbnMiOlsiUk9MRV9NT0RJRlkiLCJBVVRIT1JJVFlfTU9ESUZZIiwiVVNFUl9NT0RJRlkiLCJVU0VSX1ZJRVciLCJERUZBVUxUX1VTRVIiLCJBVVRIT1JJVFlfVklFVyIsIlJPTEVfVklFVyJdLCJpYXQiOjE2MTAzOTE1OTUsImV4cCI6MTYxMDQ3Nzk5NSwiaXNzIjoid3d3LmJvbG9naHUuY29tIiwianRpIjoiZDk1ZjIxYzMzNzM0ZDI4YzcxY2E4MjhhMmM2ZmY1NjcifQ.br0LaXlzIBuPEISWJxepV-Ei1pb2UNyWJUNixdjrtDlUKlVC0SuQ084H1G0WnOwihBnJ3qYpYqn9cNHm5igeS1H9JMlBFMlK4mfImWLqWCCc3MvgPHrJBgB8wW8H2kg7UgRu-jhvNlOJVY6tgxOcHFwNepvttN23AlPXHcwOATk"
          },
          "username": {
            "type": "string",
            "example": "admin01"
          }
        }
      },
      "Token": {
        "type": "object",
        "properties": {
          "token": {
            "type": "string",
            "example": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMGQyMmMwNDUxIiwidXNlcm5hbWUiOiJhZG1pbjAxIiwicGVybWlzc2lvbnMiOlsiUk9MRV9NT0RJRlkiLCJBVVRIT1JJVFlfTU9ESUZZIiwiVVNFUl9NT0RJRlkiLCJVU0VSX1ZJRVciLCJERUZBVUxUX1VTRVIiLCJBVVRIT1JJVFlfVklFVyIsIlJPTEVfVklFVyJdLCJpYXQiOjE2MTAzOTE1OTUsImV4cCI6MTYxMDQ3Nzk5NSwiaXNzIjoid3d3LmJvbG9naHUuY29tIiwianRpIjoiZDk1ZjIxYzMzNzM0ZDI4YzcxY2E4MjhhMmM2ZmY1NjcifQ.br0LaXlzIBuPEISWJxepV-Ei1pb2UNyWJUNixdjrtDlUKlVC0SuQ084H1G0WnOwihBnJ3qYpYqn9cNHm5igeS1H9JMlBFMlK4mfImWLqWCCc3MvgPHrJBgB8wW8H2kg7UgRu-jhvNlOJVY6tgxOcHFwNepvttN23AlPXHcwOATk"
          }
        }
      },
      "User": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "example": "455a5dbf75c"
          },
          "username": {
            "type": "string",
            "example": "admin01"
          },
          "email": {
            "type": "string",
            "example": "admin01@gmail.com"
          },
          "activated": {
            "type": "booelan",
            "example": true
          },
          "locked": {
            "type": "booelan",
            "example": true
          },
          "loginAttemptCount": {
            "type": "integer",
            "format": "int32",
            "example": 0
          },
          "passwordHash": {
            "type": "string",
            "example": "$2a$10$0FWZqemfw0I7SxVc1Hpnfu7SMrQpbB3W9N0.0qFvt5dwX0q.hqdoS"
          },
          "role": {
            "type": "object",
            "properties": {
              "id": {
                "type": "string",
                "example": "455a5dbf75c"
              },
              "name": {
                "type": "string",
                "example": "RoleTest001"
              },
              "authorities": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "string",
                      "example": "455a5dbf75c"
                    },
                    "name": {
                      "type": "string",
                      "example": "AUTHORITY_TEST_001"
                    },
                    "createdAt": {
                      "type": "string",
                      "format": "date-time",
                      "description": "Creation date and time",
                      "example": "2021-01-30T08:30:00Z"
                    },
                    "updatedAt": {
                      "type": "string",
                      "format": "date-time",
                      "description": "Update date and time",
                      "example": "2021-01-30T08:30:00Z"
                    },
                    "createdBy": {
                      "type": "string",
                      "example": "a1b2c3c2b1a,system"
                    },
                    "updatedBy": {
                      "type": "string",
                      "example": "a1b2c3c2b1a,system"
                    }
                  }
                }
              },
              "createdAt": {
                "type": "string",
                "format": "date-time",
                "description": "Creation date and time",
                "example": "2021-01-30T08:30:00Z"
              },
              "updatedAt": {
                "type": "string",
                "format": "date-time",
                "description": "Update date and time",
                "example": "2021-01-30T08:30:00Z"
              },
              "createdBy": {
                "type": "string",
                "example": "a1b2c3c2b1a,system"
              },
              "updatedBy": {
                "type": "string",
                "example": "a1b2c3c2b1a,system"
              }
            }
          },
          "createdAt": {
            "type": "string",
            "format": "date-time",
            "description": "Creation date and time",
            "example": "2021-01-30T08:30:00Z"
          },
          "updatedAt": {
            "type": "string",
            "format": "date-time",
            "description": "Update date and time",
            "example": "2021-01-30T08:30:00Z"
          },
          "createdBy": {
            "type": "string",
            "example": "a1b2c3c2b1a,system"
          },
          "updatedBy": {
            "type": "string",
            "example": "a1b2c3c2b1a,system"
          }
        }
      },
      "UserConfirmation": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "example": "0005d429d6c"
          },
          "ip": {
            "type": "string",
            "example": "127.0.0.1"
          },
          "requestDate": {
            "type": "string",
            "example": "2021-03-21T04:30:43.246Z"
          },
          "userId": {
            "type": "string",
            "example": "30005d429d6"
          }
        }
      }
    },
    "parameters": {
      "Page": {
        "in": "query",
        "name": "page",
        "required": false,
        "description": "ex. 1",
        "schema": {
          "type": "number",
          "minimum": 1,
          "example": null
        }
      }
    },
    "responses": {
      "BadRequest": {
        "description": "Validation Error",
        "content": {
          "application/json": {
            "schema": {
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "value": {
                    "type": "string",
                    "example": "USER_DEFAULT"
                  },
                  "msg": {
                    "type": "string",
                    "example": "username is required"
                  },
                  "param": {
                    "type": "string",
                    "example": "username"
                  },
                  "location": {
                    "type": "string",
                    "example": "body"
                  }
                }
              }
            }
          }
        }
      },
      "InternalServerError": {
        "description": "Internal Server Error",
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "properties": {
                "msg": {
                  "type": "string",
                  "example": "errors is not defined"
                }
              }
            }
          }
        }
      },
      "NotFound": {
        "description": "The specified resource was not found",
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "properties": {
                "msg": {
                  "type": "string",
                  "example": "http://localhost:3001/api/authorities/0dd83546658 is not found"
                }
              }
            }
          }
        }
      },
      "Unauthorized": {
        "description": "Unauthorized",
        "content": {
          "application/json": {
            "schema": {
              "type": "object",
              "properties": {
                "msg": {
                  "type": "string",
                  "example": "No authorization token was found"
                }
              }
            }
          }
        }
      }
    },
    "securitySchemes": {
      "bearerAuth": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    }
  }
}
