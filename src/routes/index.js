require('module-alias/register');

const indexRouter = require('@routes/index-route');

const securityRestRouter = require('@routes/rest/security-rest-route');
const authorityRestRouter = require('@routes/rest/authority-rest-route');
const roleRestRouter = require('@routes/rest/role-rest-route');
const userRestRouter = require('@routes/rest/user-rest-route');
const tagRestRouter = require('@routes/rest/tag-rest-route');
const postRestRouter = require('@routes/rest/post-rest-route');
const imageRestRouter = require('@routes/rest/image-rest-route');

const logCreateRestRouter = require('@routes/rest/log-create-rest-route');
const logDeleteRestRouter = require('@routes/rest/log-delete-rest-route');
const logPostRestRouter = require('@routes/rest/log-post-rest-route');
const logSecurityRestRouter = require('@routes/rest/log-security-rest-route');
const logUpdateRestRouter = require('@routes/rest/log-update-rest-route');

module.exports = function (app) {

    app.use('/', indexRouter);
    app.use('/api', securityRestRouter);
    app.use('/api/authorities', authorityRestRouter);
    app.use('/api/roles', roleRestRouter);
    app.use('/api/users', userRestRouter);
    app.use('/api/tags', tagRestRouter);
    app.use('/api/posts', postRestRouter);
    app.use('/api/images', imageRestRouter);

    app.use('/api/logcreates', logCreateRestRouter);
    app.use('/api/logdeletes', logDeleteRestRouter);
    app.use('/api/logposts', logPostRestRouter);
    app.use('/api/logsecurities', logSecurityRestRouter);
    app.use('/api/logupdates', logUpdateRestRouter);

};
