require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const postValidation = require('@validations/post-validation');
const logCreateService = require('@services/log-create-service');
const logUpdateService = require('@services/log-update-service');
const logDeleteService = require('@services/log-delete-service');
const logPostService = require('@services/log-post-service');
const postService = require('@services/post-service');
const uploadService = require("@services/upload-service");


var router = express.Router();
router.get('/', logPostService.log, findAll);
router.get('/tags/:tag', logPostService.log, findAllByTag);
router.get('/random', findRandom);
router.get('/:id', findById);
router.post('/', uploadService.single(), logCreateService.log, guard.check('POST_MODIFY'), postValidation.create, create);
router.put('/:id', uploadService.single(), logUpdateService.log, guard.check('POST_MODIFY'), postValidation.update, update);
router.delete('/:id', logDeleteService.log, guard.check('POST_MODIFY'), destroy);
router.get('/view/:code', logPostService.log, view);

async function findAll(req, res, next) {
  try {
    let result = await postService.findAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findAllByTag(req, res, next) {
  try {
    let result = await postService.findAllByTag(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findRandom(req, res, next) {
  try {
    let result = await postService.findRandom(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findById(req, res, next) {
  try {
    let post = await postService.findById(req);
    res.status(200).json(post);
  } catch (error) {
    return next(error)
  }
}

async function view(req, res, next) {
  try {
    let result = await postService.findByCode(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      uploadService.clear(req);
      res.status(400).json({ errors: errors.array() });
    } else {
      let post = await postService.create(req);
      uploadService.clear(req);
      res.status(201).json(post);
    }
  } catch (error) {
    uploadService.clear(req);
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      uploadService.clear(req);
      res.status(400).json({ errors: errors.array() });
    } else {
      let post = await postService.update(req);
      uploadService.clear(req);
      res.status(200).json(post);
    }
  } catch (error) {
    uploadService.clear(req);
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    let post = await postService.destroy(req);
    res.set('Message', `${req.params.id} is deleted`);
    res.status(204).json(post);
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
