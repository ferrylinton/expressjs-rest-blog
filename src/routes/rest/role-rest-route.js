require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const logCreateService = require('@services/log-create-service');
const logUpdateService = require('@services/log-update-service');
const logDeleteService = require('@services/log-delete-service');
const roleService = require("@services/role-service");
const roleValidation = require('@validations/role-validation');


var router = express.Router();
router.get('/', guard.check('ROLE_VIEW'), findAndCountAll);
router.get('/all', guard.check('ROLE_VIEW'), getAll);
router.get('/:id', guard.check('ROLE_VIEW'), findOne);
router.post('/', logCreateService.log, guard.check('ROLE_MODIFY'), roleValidation.create, create);
router.put('/:id', logUpdateService.log, guard.check('ROLE_MODIFY'), roleValidation.update, update);
router.delete('/:id', logDeleteService.log, guard.check('ROLE_MODIFY'), destroy);


async function getAll(req, res, next) {
  try {
    let result = await roleService.getAll();
    res.status(200).json(result);
  } catch (err) {
    return next(err)
  }
}

async function findAndCountAll(req, res, next) {
  try {
    var result = await roleService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findOne(req, res, next) {
  try {
    const role = await roleService.findOne(req);
    res.status(200).json(role);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const role = await roleService.create(req);
      res.status(201).json(role);
    }
  } catch (error) {
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const role = await roleService.update(req);
      res.status(200).json(role);
    }
  } catch (error) {
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    let role = await roleService.destroy(req);
    res.set('Message', `${req.params.id} is deleted`);
    res.status(204).json(role);
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
