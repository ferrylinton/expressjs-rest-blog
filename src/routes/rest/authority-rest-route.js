require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const logCreateService = require('@services/log-create-service');
const logUpdateService = require('@services/log-update-service');
const logDeleteService = require('@services/log-delete-service');
const authorityService = require('@services/authority-service');
const authorityValidation = require('@validations/authority-validation');

var router = express.Router();
router.get('/', guard.check('AUTHORITY_VIEW'), findAndCountAll);
router.get('/all', guard.check('AUTHORITY_VIEW'), getAll);
router.get('/:id', guard.check('AUTHORITY_VIEW'), findOne);
router.post('/', logCreateService.log, guard.check('AUTHORITY_MODIFY'), authorityValidation.validate, create);
router.put('/:id', logUpdateService.log, guard.check('AUTHORITY_MODIFY'), authorityValidation.validate, update);
router.delete('/:id', logDeleteService.log, guard.check('AUTHORITY_MODIFY'), destroy);


async function getAll(req, res, next) {
  try {
    let result = await authorityService.getAll();
    res.status(200).json(result);
  } catch (err) {
    return next(err)
  }
}

async function findAndCountAll(req, res, next) {
  try {
    let result = await authorityService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (err) {
    return next(err)
  }
}

async function findOne(req, res, next) {
  try {
    let authority = await authorityService.findOne(req);
    res.status(200).json(authority);
  } catch (err) {
    return next(err)
  }
}

async function create(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let authority = await authorityService.create(req);
      res.status(201).json(authority);
    }
  } catch (err) {
    return next(err)
  }
}

async function update(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let authority = await authorityService.update(req);
      res.status(200).json(authority);
    }
  } catch (err) {
    return next(err)
  }
}

async function destroy(req, res, next) {
  try {
    let authority = await authorityService.destroy(req);
    res.set('Message', `${req.params.id} is deleted`);
    res.status(204).json(authority);
  } catch (err) {
    return next(err)
  }
}

module.exports = router;
