require('module-alias/register');

const express = require('express');
const { validationResult } = require('express-validator');
const securityService = require('@services/security-service');
const logSecurityService = require('@services/log-security-service');
const userValidation = require('@validations/user-validation');
const errorUtil = require('@utils/error-util')


var router = express.Router();
router.post('/authenticate', logSecurityService.log, userValidation.authenticate, authenticate);
router.get('/refresh', logSecurityService.log, refresh);
router.get('/revoke', logSecurityService.log, revoke);

router.post('/password', userValidation.changePassword, changePassword);
router.post('/register', userValidation.create, register);


async function authenticate(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorUtil.getValidationErrors(errors, res);
    } else {
      let token = await securityService.authenticate(req);
      return res.status(200).json(token);
    }
  } catch (error) {
    return next(error)
  }
}

async function refresh(req, res, next) {
  try {
    let token = await securityService.refresh(req);
    return res.status(200).json(token);
  } catch (error) {
    return next(error)
  }
}

async function changePassword(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      return errorUtil.getValidationErrors(errors, res);
    } else {
      await securityService.changePassword(req);
      return res.status(200).json({ msg: 'Change password is successfully' });
    }
  } catch (error) {
    return next(error)
  }

}

async function revoke(req, res, next) {
  try {
    let token = await securityService.revoke(req);
    res.status(200).json(token);
  } catch (error) {
    return next(error)
  }
}

async function register(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const user = await securityService.register(req);
      res.status(201).json(user);
    }
  } catch (error) {
    return next(error)
  }
}


module.exports = router;
