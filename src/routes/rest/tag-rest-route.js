require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const logCreateService = require('@services/log-create-service');
const logUpdateService = require('@services/log-update-service');
const logDeleteService = require('@services/log-delete-service');
const tagService = require('@services/tag-service');
const tagValidation = require('@validations/tag-validation');

var router = express.Router();
router.get('/', findAndCountAll);
router.get('/all', getAll);
router.get('/:id', findOne);
router.post('/', logCreateService.log, guard.check('TAG_MODIFY'), tagValidation.validate, create);
router.put('/:id', logUpdateService.log, guard.check('TAG_MODIFY'), tagValidation.validate, update);
router.delete('/:id', logDeleteService.log, guard.check('TAG_MODIFY'), destroy);


async function getAll(req, res, next) {
  try {
    let result = await tagService.getAll();
    res.status(200).json(result);
  } catch (err) {
    return next(err)
  }
}

async function findAndCountAll(req, res, next) {
  try {
    let result = await tagService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (err) {
    return next(err)
  }
}

async function findOne(req, res, next) {
  try {
    let tag = await tagService.findOne(req);
    res.status(200).json(tag);
  } catch (err) {
    return next(err)
  }
}

async function create(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let tag = await tagService.create(req);
      res.status(201).json(tag);
    }
  } catch (err) {
    return next(err)
  }
}

async function update(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let tag = await tagService.update(req);
      res.status(200).json(tag);
    }
  } catch (err) {
    return next(err)
  }
}

async function destroy(req, res, next) {
  try {
    let tag = await tagService.destroy(req);
    res.set('Message', `${req.params.id} is deleted`);
    res.status(204).json(tag);
  } catch (err) {
    return next(err)
  }
}

module.exports = router;
