require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const logCreateService = require('@services/log-create-service');
const logUpdateService = require('@services/log-update-service');
const logDeleteService = require('@services/log-delete-service');
const userValidation = require('@validations/user-validation');
const userService = require('@services/user-service');


var router = express.Router();
router.get('/', guard.check('USER_VIEW'), findAndCountAll);
router.get('/:id', guard.check('USER_VIEW'), findOne);
router.post('/', logCreateService.log, guard.check('USER_MODIFY'), userValidation.create, create);
router.put('/:id', logUpdateService.log, guard.check('USER_MODIFY'), userValidation.update, update);
router.delete('/:id', logDeleteService.log, guard.check('USER_MODIFY'), destroy);


async function findAndCountAll(req, res, next) {
  try {
    var result = await userService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findOne(req, res, next) {
  try {
    const user = await userService.findOne(req);
    res.status(200).json(user);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const user = await userService.create(req);
      res.status(201).json(user);
    }
  } catch (error) {
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      const user = await userService.update(req);
      res.status(200).json(user);
    }
  } catch (error) {
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    let user = await userService.destroy(req);
    res.set('Message', `${req.params.id} is deleted`);
    res.status(204).send(user);
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
