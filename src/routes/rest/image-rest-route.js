require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const logCreateService = require('@services/log-create-service');
const logUpdateService = require('@services/log-update-service');
const logDeleteService = require('@services/log-delete-service');
const uploadService = require('@services/upload-service');
const imageService = require('@services/image-service');
const imageValidation = require('@validations/image-validation');


var router = express.Router();
router.get('/', findAndCountAll);
router.get('/:id', findOne);
router.get('/view/:name', view);
router.post('/', uploadService.single(), logCreateService.log, guard.check('IMAGE_MODIFY'), imageValidation.create, create);
router.put('/:id', uploadService.single(), logUpdateService.log, guard.check('IMAGE_MODIFY'), imageValidation.update, update);
router.delete('/:id', logDeleteService.log, guard.check('IMAGE_MODIFY'), destroy);

async function findAndCountAll(req, res, next) {
  try {
    let result = await imageService.findAndCountAll(req);
    res.status(200).json(result);
  } catch (error) {
    return next(error)
  }
}

async function findOne(req, res, next) {
  try {
    let image = await imageService.findOne(req);
    res.status(200).json(image);
  } catch (error) {
    return next(error)
  }
}

async function view(req, res, next) {
  try {
    let image = await imageService.view(req);
    let buffer = Buffer.from(image.bytes, 'base64');

    res.writeHead(200, {
      'Content-Type': image.contentType,
      'Content-Length': buffer.length
    });

    res.end(buffer);
  } catch (error) {
    return next(error)
  }
}

async function create(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      uploadService.clear(req);
      res.status(400).json({ errors: errors.array() });
    } else {
      let image = await imageService.create(req);
      res.status(201).json(image);
    }
  } catch (error) {
    uploadService.clear(req);
    return next(error)
  }
}

async function update(req, res, next) {
  try {
    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      uploadService.clear(req);
      res.status(400).json({ errors: errors.array() });
    } else {
      let image = await imageService.update(req);
      uploadService.clear(req);
      res.status(200).json(image);
    }
  } catch (error) {
    uploadService.clear(req);
    return next(error)
  }
}

async function destroy(req, res, next) {
  try {
    await imageService.destroy(req);
    res.set('Message', `${req.params.id} is deleted`);
    res.status(204).send();
  } catch (error) {
    return next(error)
  }
}

module.exports = router;
