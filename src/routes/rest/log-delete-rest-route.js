require('module-alias/register');

const express = require('express');
const guard = require('express-jwt-permissions')();
const { validationResult } = require('express-validator');
const logDeleteService = require('@services/log-delete-service');
const { checkPage, checkNumber } = require('@validations/params-validation');


var router = express.Router();
router.get('/', guard.check('LOG_AUDIT_VIEW'), checkPage, findAndCountAll);
router.get('/:id', guard.check('LOG_AUDIT_VIEW'), findOne);
router.delete('/', guard.check('LOG_AUDIT_VIEW'), checkNumber, destroy);

async function findAndCountAll(req, res, next) {
  try {

    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      let result = await logDeleteService.findAndCountAll(req);
      res.status(200).json(result);
    }
    
  } catch (err) {
    return next(err)
  }
}

async function findOne(req, res, next) {
  try {
    let logDelete = await logDeleteService.findOne(req);
    res.status(200).json(logDelete);
  } catch (err) {
    return next(err)
  }
}

async function destroy(req, res, next) {
  try {

    let errors = validationResult(req);

    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() });
    } else {
      await logDeleteService.destroy(req);
      res.set('Message', `Amount of deleted data is ${req.query.number}`);
      res.status(204).send();
    }
    
  } catch (err) {
    return next(err)
  }
}

module.exports = router;
