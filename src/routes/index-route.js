require('module-alias/register');

const express = require('express');
const userConfirmationService = require('@services/user-confirmation-service');


var router = express.Router();

router.get('/', (req, res) => {
    res.redirect('/api-docs');
});
router.get('/users/confirm/:id', userConfirmationService.confirm);
router.get('/users/confirm/resend/:id', userConfirmationService.resend);

module.exports = router;