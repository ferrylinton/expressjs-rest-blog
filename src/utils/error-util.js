require('module-alias/register');

const _ = require('lodash');
const NotFoundError = require('@errors/not-found-error');


function getValidationErrors(errors, res) {
    return res.status(400).json(errors.array().map(error => {
        return {
            "msg": error.msg,
            "param": error.param
        }
    }));
}

function throwNotFoundError(obj) {
    if (_.isString(obj)) {
        throw new NotFoundError(`${obj} is not found`);
    } else {
        throw new NotFoundError(`${obj.protocol}://${obj.headers.host}${obj.originalUrl} is not found`);
    }
}

module.exports = {
    getValidationErrors,
    throwNotFoundError
};