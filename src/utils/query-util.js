const _ = require('lodash');
const { Sequelize, Op } = require('sequelize');
const { where, fn, col } = Sequelize;
const logger = require('@configs/logger');
const PAGE_SIZE = 5;
const MULTIPLE_VALUES = [ 'between', 'notBetween', 'in', 'notIn', 'overlap', 'contains', 'contained', 'any' ]

function getParams(model, req, order) {
    let params = { order }
    setParams(model, req, params);
    return params;
}

function setParams(model, req, params) {
    setFilters(model, req, params);
    setOrders(model, req, params);
    setLimit(req, params);
}

function setLimit(req, params) {
    params.current = _.toInteger(req.query.page);
    params.current = params.current > 0 ? params.current : 1;
    params.offset = (params.current - 1) * PAGE_SIZE;
    params.limit = PAGE_SIZE;
}

function setOrders(model, req, params) {
    let result = [];

    if (req.query.order) {
        let orders = req.query.order.trim().split(',');

        orders.forEach(function (order) {
            let str = order.trim().split('|')

            if (str.length === 1 && Object.keys(model.rawAttributes).includes(str[0])) {
                result.push([str[0], 'ASC']);
            } else if (str.length === 2 && Object.keys(model.rawAttributes).includes(str[0])) {
                result.push([str[0], (str[1].toLowerCase() === 'asc' ? 'ASC' : 'DESC')]);
            }
        });
    }

    if (result.length > 0) {
        params.order = result;
    }
}

function setFilters(model, req, params) {
    let filters = [];

    for (let fieldName in model.rawAttributes) {
        if (req.query.hasOwnProperty(fieldName)) {

            // query parameter's name is converted to field using model.rawAttributes
            let field = `${model.name}.${model.rawAttributes[fieldName].field}`;

            // field's type using model.rawAttributes
            let type = model.rawAttributes[fieldName].type.key;

            let columnOp = getColumnOp(req.query[fieldName], type);

            let columnValue = getColumnValue(req.query[fieldName], columnOp, type);

            setSequelizeWhere(filters, field, columnOp, columnValue, type);
        }
    }

    if (filters.length > 1) {
        let queryOp = (req.query.op && req.query.op.toLowerCase() === 'or') ? Op.or : Op.and;
        params.where = { [queryOp]: filters }
    } else if (filters.length > 0) {
        params.where = filters[0]
    }
}

function getColumnOp(parameter, type) {
    let values = parameter.trim().split('|');

    if (values.length === 1) {
        if ((type === 'STRING' || type === 'TEXT')) {
            return 'substring';
        } else {
            return 'eq';
        }
    }

    if (values.length === 2 && Object.keys(Op).includes(values[0])) {
        return values[0];
    }

    return '';
}

function getColumnValue(parameter, columnOp, type) {
    let values = parameter.split('|');

    if (values.length === 1 && parameter.length > 0) {
        return getSingleValue(parameter, type);
    } else if (values.length === 2 && Object.keys(Op).includes(columnOp)) {
        let arr = values[1].split(',');

        if (arr.length === 1 && values[1].length > 1 && !MULTIPLE_VALUES.includes(columnOp)) {
            return getSingleValue(values[1].trim(), type);
        } else if (arr.length > 1 && MULTIPLE_VALUES.includes(columnOp)) {
            return getMultiValues(arr, type);
        }
    }

    return '';
}

function getSingleValue(parameter, type) {
    if ((type === 'STRING' || type === 'TEXT') && _.isString(parameter)) {
        return parameter.toLowerCase();
    } else if (type === 'DATE' && isValidDate(parameter)) {
        return parameter
    } else if (type === 'DATEONLY' && isValidDate(parameter)) {
        return parameter
    } else if (type === 'INTEGER' && !isNaN(parameter)) {
        return parseInt(parameter)
    }
}

function getMultiValues(arr, type) {
    if (type === 'INTEGER') {
        let result = [];

        arr.forEach(function (val) {
            if (!isNaN(val)) {
                result.push(parseInt(val))
            }
        });

        return result;
    } else if (type === 'DATE') {
        let result = [];

        arr.forEach(function (val) {
            if (isValidDate(val)) {
                result.push(val)
            }
        });

        return result;
    } else if (type === 'STRING' || type === 'TEXT') {
        return arr;
    }

    return '';
}

function setSequelizeWhere(filters, field, columnOp, columnValue, type) {
    logger.debug(`setSequelizeWhere [field=${field}, columnOp=${columnOp}, columnValue=${columnValue}, type=${type}]`);
    
    if (columnValue === '') {
        return;
    }

    if ((type === 'STRING' || type === 'TEXT')) {
        if (_.isString(columnValue) && (columnOp === 'like' || columnOp === 'substring' || columnOp === 'notLike')) {

            if (columnOp === 'like' || columnOp === 'notLike') {
                columnValue = '%' + columnValue + '%';
            }
            filters.push(where(fn('lower', col(field)), { [Op[columnOp]]: columnValue }));

        } else {
            filters.push(where(col(field), { [Op[columnOp]]: columnValue }));
        }

    } else if (type === 'DATE') {
        filters.push(where(fn('date', col(field)), { [Op[columnOp]]: columnValue }));
    } else if (type === 'DATEONLY') {
        filters.push(where(col(field), { [Op[columnOp]]: columnValue }));
    } else if (type === 'INTEGER') {
        filters.push(where(col(field), { [Op[columnOp]]: columnValue }));
    }
}



function getPagination(req, count, params) {
    let pagination = {};

    pagination.size = params.limit;
    pagination.current = params.current;
    pagination.total = Math.ceil(count / params.limit);
    pagination.hasPrevious = params.current > 1;
    pagination.hasNext = params.current < pagination.total;

    if (pagination.hasPrevious) {
        pagination.previousUrl = getQueryString(req, params.current - 1);
    }

    if (pagination.hasNext) {
        pagination.nextUrl = getQueryString(req, params.current + 1);
    }

    return pagination;
}

function getQueryString(req, page) {
    req.query.page = page;

    let entries = Object.entries(req.query).map(entry => {
        return entry.join('=')
    });

    return '?' + entries.join('&');
}

function isValidDate(date) {
    var pattern = /^\d{4}-\d{2}-\d{2}$/;
    return date.match(pattern);
}

function getFilters(params) {
    if (params.where) {

        if (params.where[Op.or]) {
            return params.where[Op.or];
        } else if (params.where[Op.and]) {
            return params.where[Op.and];
        }

        return [params.where];
    }
}

module.exports = {
    getParams,
    setParams,
    setLimit,
    setOrders,
    setFilters,
    getPagination,
    getFilters
};