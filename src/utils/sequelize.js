'use strict';

const { Sequelize } = require('sequelize');

function defaultDatetime() {
  let dialect = (process.env.NODE_ENV === 'test') ? process.env.TEST_DIALECT : process.env.SEQUELIZE_DIALECT;

  if (dialect === 'sqlite') {
    return Sequelize.literal('CURRENT_TIMESTAMP');
  } else if (dialect === 'mysql' || dialect === 'mariadb' || dialect === 'postgres') {
    return Sequelize.fn('now');
  } else {
    return Sequelize.fn('now')
  }
}

function defaultBoolean() {
  let dialect = (process.env.NODE_ENV === 'test') ? process.env.TEST_DIALECT : process.env.SEQUELIZE_DIALECT;

  if (dialect === 'mysql' || dialect === 'mariadb' || dialect === 'sqlite') {
    return 0;
  } else if (dialect === 'postgres') {
    return false;
  } else {
    return false
  }
}

function formatDate() {
  let dialect = (process.env.NODE_ENV === 'test') ? process.env.TEST_DIALECT : process.env.SEQUELIZE_DIALECT;

  if (dialect === 'mysql' || dialect === 'mariadb' || dialect === 'postgres') {
    return 'date_format';
  } else if (dialect === 'strftime') {
    return 'date_format';
  } else {
    return 'date_format'
  }
}

module.exports = {
  defaultDatetime,
  defaultBoolean,
  formatDate
};