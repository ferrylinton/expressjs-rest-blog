require('module-alias/register');

const path = require('path');
const fs = require('fs');
const jwt = require('express-jwt');
const { pathToRegexp } = require("path-to-regexp");
const { RevokedToken } = require('@models/index');


module.exports = function (app) {

    var isRevokedCallback = async function (req, payload, done) {
        try {
            let token = await RevokedToken.findOne({
                where: { id: payload.jti }
            });

            if (token) {
                return done(null, true);
            } else {
                return done(null, false);
            }

        } catch (error) {
            return done(error)
        }
    };

    var checkJwt = jwt({
        secret: fs.readFileSync(path.join(process.cwd(), 'security-keys', 'public.pub'), 'utf8'),
        isRevoked: isRevokedCallback,
        credentialsRequired: true,
        algorithms: ['RS256'],
    }).unless({
        path: [
            '/',
            '/images/favicon.png',
            '/favicon.ico',
            '/api/authenticate',
            '/api/register',
            { url: '/api/posts', methods: ['GET'] },
            { url: '/api/tags', methods: ['GET'] },
            { url: '/api/tags/all', methods: ['GET'] },
            { url: '/api/images', methods: ['GET'] },
            { url: pathToRegexp('/api/posts/:id'), methods: ['GET'] },
            { url: pathToRegexp('/api/posts/view/:code'), methods: ['GET'] },
            { url: pathToRegexp('/api/posts/random/:by'), methods: ['GET'] },
            { url: pathToRegexp('/api/posts/tags/:tag'), methods: ['GET'] },
            { url: pathToRegexp('/api/images/:id'), methods: ['GET'] },
            { url: pathToRegexp('/api/images/view/:name'), methods: ['GET'] },
            { url: pathToRegexp('/api/tags/:id'), methods: ['GET'] },
            { url: pathToRegexp('/users/confirm/:id'), methods: ['GET'] },
            { url: pathToRegexp('/users/confirm/resend/:id'), methods: ['GET'] },
            { url: pathToRegexp('/users/password/:id'), methods: ['GET'] }
        ]
    });

    app.use(checkJwt);

};
