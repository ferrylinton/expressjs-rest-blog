
require('module-alias/register');
const logger = require('@configs/logger');


module.exports = (process.env.NODE_ENV === 'test') ?
  {
    username: process.env.TEST_USERNAME,
    password: process.env.TEST_PASSWORD,
    database: process.env.TEST_DATABASE,
    host: process.env.TEST_HOST,
    port: parseInt(process.env.TEST_PORT),
    storage: process.env.TEST_STORAGE,
    dialect: process.env.TEST_DIALECT,
    logging: false,
    define: {
      timestamps: false,
      underscored: true
    }
  } :
  {
    username: process.env.SEQUELIZE_USERNAME,
    password: process.env.SEQUELIZE_PASSWORD,
    database: process.env.SEQUELIZE_DATABASE,
    host: process.env.SEQUELIZE_HOST,
    port: parseInt(process.env.SEQUELIZE_PORT),
    storage: process.env.SEQUELIZE_STORAGE,
    dialect: process.env.SEQUELIZE_DIALECT,
    logging: (msg) => logger.debug(msg),
    define: {
      timestamps: false,
      underscored: true
    },
    migrationStorage: 'sequelize',
    migrationStorageTableName: 'sequelize_meta',
    seederStorage: 'sequelize',
    seederStorageTableName: 'sequelize_data'
  };