require('module-alias/register');

const path = require('path');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const SWAGGER_FOLDER = path.join(process.cwd(), 'src', 'swagger-definitions');

module.exports = function (app) {

    var options = {
        explorer: true,
        customCssUrl: '/css/swagger-ui-custom.css',
        customSiteTitle: 'Bologhu REST API',
        customfavIcon: '/img/favicon.png',
        customJs: '/js/swagger-ui-custom.js',
        swaggerOptions: {
            urls: [
                {
                    url: process.env.BASE_URL + '/security.json',
                    name: 'security'
                },
                {
                    url: process.env.BASE_URL + '/blog.json',
                    name: 'blog'
                },
                {
                    url: process.env.BASE_URL + '/log.json',
                    name: 'log'
                },
                {
                    url: process.env.BASE_URL + '/usermanagement.json',
                    name: 'usermanagement'
                }
            ]
        }

    };

    app.use((req, res, next) => {
        if (req.url === '/security.json') {
            res.sendFile(path.join(SWAGGER_FOLDER, 'security.json'));
        } else if (req.url === '/blog.json') {
            res.sendFile(path.join(SWAGGER_FOLDER, 'blog.json'));
        } else if (req.url === '/log.json') {
            res.sendFile(path.join(SWAGGER_FOLDER, 'log.json'));
        } else if (req.url === '/usermanagement.json') {
            res.sendFile(path.join(SWAGGER_FOLDER, 'usermanagement.json'));;
        } else {
            next();
        }
    });

    app.use(express.static(path.join(__dirname, 'public')));
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(null, options));

};
