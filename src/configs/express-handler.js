require('module-alias/register');

const createError = require('http-errors');
const logger = require('@configs/logger');

module.exports = function (app) {

    app.use(function (req, res, next) {
        next(createError(404));
    });

    app.use(function (err, req, res, next) {
        logger.error((err) ? err.stack : err);

        if (err.name === 'MulterError' && err.code === 'LIMIT_UNEXPECTED_FILE') {
            res.status(400);
            res.json({ "msg": `${err.name} : ${err.message} ['${err.field}']` });
        } else if (err.errors) {
            let messages = [];

            err.errors.forEach(function (element, index) {
                if (element.constructor.name === 'ValidationErrorItem') {
                    messages.push({
                        param: element.path,
                        msg: element.message,
                        value: element.value
                    });
                }
            });

            res.status(400);
            res.json(messages);
        } else {
            res.status(err.status || 500);
            res.json({ "msg": err.message });
        }

    });

};
