require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { audit } = require('@columns/commons/audit');

const tableName = 'blg_tag';

const attributes = {

  id: {
    type: DataTypes.STRING(11),
    primaryKey: true
  },

  name: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: true
  }

}

function camelCase() {
  return _.merge({}, attributes, audit);
}

function snakeCase() {
  let tempAttributes = _.merge({}, attributes, audit);
  let newAttributes = {};

  for (var key in tempAttributes) {
    newAttributes[_.snakeCase(key)] = tempAttributes[key];
  }

  return newAttributes;
}

module.exports = {
  tableName,
  camelCase,
  snakeCase
};