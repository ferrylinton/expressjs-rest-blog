require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const log = require('@columns/commons/log');

const modelName = 'LogPost';

const tableName = 'log_post';

const attributes = {

  username: {
    type: DataTypes.STRING(50),
    allowNull: true,
  },

  params: {
    type: DataTypes.STRING,
    allowNull: true
  }

}

function camelCase() {
  return _.merge({}, log, attributes);
}

function snakeCase() {
  let tempAttributes = _.merge({}, log, attributes);
  let newAttributes = {};

  for (var key in tempAttributes) {
    newAttributes[_.snakeCase(key)] = tempAttributes[key];
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};