require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { defaultDatetime } = require('@utils/sequelize');

const create = {

  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: defaultDatetime()
  },

  updatedAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: defaultDatetime()
  }

}

const update = {

  createdBy: {
    type: DataTypes.STRING(70),
    allowNull: false,
    defaultValue: process.env.DEFAULT_USER
  },

  updatedBy: {
    type: DataTypes.STRING(70),
    allowNull: false,
    defaultValue: process.env.DEFAULT_USER
  }

}

const audit = _.merge({}, create, update);

module.exports = {
  create,
  audit
};