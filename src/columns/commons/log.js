require('module-alias/register');

const { DataTypes } = require('sequelize');
const { defaultDatetime } = require('@utils/sequelize');

module.exports = {

  id: {
    type: DataTypes.STRING(30),
    primaryKey: true
  },

  ip: {
    type: DataTypes.STRING(20),
    allowNull: true
  },

  userAgent: {
    type: DataTypes.STRING,
    allowNull: true
  },

  host: {
    type: DataTypes.STRING(30),
    allowNull: true,
  },

  path: {
    type: DataTypes.STRING(50),
    allowNull: true,
  },

  method: {
    type: DataTypes.STRING(10),
    allowNull: true,
  },

  requestDate: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: defaultDatetime()
  }

};