require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const tag = require('@columns/tag');
const post = require('@columns/post');
const { create } = require('@columns/commons/audit');

const tableName = 'blg_tag_post';

const attributes = {

  tagId: {
    type: DataTypes.STRING(11),
    allowNull: false,
    references: {
      model: tag.tableName,
      key: 'id',
    }
  },

  postId: {
    type: DataTypes.STRING(100),
    allowNull: false,
    references: {
      model: post.tableName,
      key: 'id',
    }
  }

}

function camelCase() {
  return _.merge({}, attributes, create);
}

function snakeCase() {
  let tempAttributes = _.merge({}, attributes, create);
  let newAttributes = {};

  for (var key in tempAttributes) {
    newAttributes[_.snakeCase(key)] = tempAttributes[key];
  }

  return newAttributes;
}

module.exports = {
  tableName,
  camelCase,
  snakeCase
};
