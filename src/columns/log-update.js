require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const log = require('@columns/commons/log');

const modelName = 'LogUpdate';

const tableName = 'log_update';

const attributes = {

  params: {
    type: DataTypes.STRING,
    allowNull: true
  },

  username: {
    type: DataTypes.STRING(50),
    allowNull: true,
  },

  statusCode: {
    type: DataTypes.INTEGER,
    allowNull: true,
  },

  requestBody: {
    type: DataTypes.TEXT,
    allowNull: true
  },

  responseBody: {
    type: DataTypes.TEXT,
    allowNull: true
  }
  
}

function camelCase() {
  return _.merge({}, log, attributes);
}

function snakeCase() {
  let tempAttributes = _.merge({}, log, attributes);
  let newAttributes = {};

  for (var key in tempAttributes) {
    newAttributes[_.snakeCase(key)] = tempAttributes[key];
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};