require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { audit } = require('@columns/commons/audit');

const modelName = 'Post';

const tableName = 'blg_post';

const attributes = {

  id: {
    type: DataTypes.STRING(11),
    primaryKey: true
  },

  code: {
    type: DataTypes.STRING(100),
    allowNull: false,
    unique: true
  },

  step: {
    type: DataTypes.INTEGER,
    allowNull: true,
    defaultValue: 999
  },
  
  title: {
    type: DataTypes.STRING(255),
    allowNull: false,
    unique: true
  },

  tagNames: {
    type: DataTypes.STRING,
    allowNull: false
  },

  description: {
    type: DataTypes.TEXT,
    allowNull: false
  },

  content: {
    type: DataTypes.TEXT,
    allowNull: false
  }

}

function camelCase() {
  return _.merge({}, attributes, audit);
}

function snakeCase() {
  let tempAttributes = _.merge({}, attributes, audit);
  let newAttributes = {};

  for (var key in tempAttributes) {
    if(tempAttributes[key].type !== DataTypes.VIRTUAL){
      newAttributes[_.snakeCase(key)] = tempAttributes[key];
    }
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};
