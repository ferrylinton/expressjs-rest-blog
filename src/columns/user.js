require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { audit } = require('@columns/commons/audit');
const { defaultBoolean } = require('@utils/sequelize');

const modelName = 'User';

const tableName = 'sec_user';

const attributes = {

  id: {
    type: DataTypes.STRING(11),
    primaryKey: true
  },

  activated: {
    type: DataTypes.BOOLEAN,
    defaultValue: defaultBoolean()
  },

  locked: {
    type: DataTypes.BOOLEAN,
    defaultValue: defaultBoolean()
  },

  loginAttemptCount: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },

  username: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: true
  },

  email: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: true
  },

  passwordHash: {
    type: DataTypes.STRING,
    allowNull: false
  },

  roleId: {
    type: DataTypes.STRING(11),
    allowNull: false
  }

}

function camelCase() {
  return _.merge({}, attributes, audit);
}

function snakeCase() {
  let tempAttributes = _.merge({}, attributes, audit);
  let newAttributes = {};

  for (var key in tempAttributes) {
    newAttributes[_.snakeCase(key)] = tempAttributes[key];
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};