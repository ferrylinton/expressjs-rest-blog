require('module-alias/register');

const { query } = require('express-validator');

module.exports = {

  checkNumber: [query('number', 'min 1 and max 100').optional().trim().isInt({ min: 1, max: 100 })],

  checkPage: [query('page', 'min 1').optional().trim().isInt({ min: 1 })]
  
};
