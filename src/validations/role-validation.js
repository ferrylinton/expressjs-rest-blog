require('module-alias/register');

const { check } = require('express-validator');
const { Role } = require('@models/index');


var create = [
  check("name", "name is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().matches(/^[A-Za-z0-9_-]+$/).withMessage("must contains alphanumeric, -, or _")
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
    .custom(isNameExist),
  check("authorities", "authorities is required")
    .isArray({ min: 1 })
]

var update = [
  check("name", "name is required")
    .optional()
    .trim()
    .escape()
    .notEmpty()
    .bail().matches(/^[A-Za-z0-9_-]+$/).withMessage("must contains alphanumeric, -, or _")
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
    .custom(isNameExist),
  check("authorities", "authorities is must be an array of string")
    .optional()
    .isArray({ min: 1 })
]

async function isNameExist(name, { req }) {
  let role = await Role.findOne({
    where: { name }
  });

  if (role) {
    if (!(req.params.id && (req.params.id == role.id))) {
      return Promise.reject(`${name} is already exist`);
    }
  }

  return true;
}

module.exports = {
  create,
  update
};
