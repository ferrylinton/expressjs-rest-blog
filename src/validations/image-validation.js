require('module-alias/register');

const { check } = require('express-validator');
const { Image } = require('@models/index');


var create = [
  check('name', 'name is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50')
    .bail().custom(isNameExist),
  check('file')
    .custom(checkFile).withMessage('image file is required')
]

var update = [
  check('name')
    .optional()
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50')
    .bail().custom(isNameExist),
  check('file')
    .optional()
    .custom(checkFile).withMessage('image file is required')
]

async function isNameExist(name, { req }) {
  let image = await Image.findOne({
    where: { name }
  });

  if (image) {
    if (!(req.params.id && (req.params.id == image.id))) {
      return Promise.reject(`${name} is already exist`);
    }
  }

  return true;
}

function checkFile(value, { req }) {
  if (req.file && req.file.mimetype.startsWith('image')) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  create,
  update
};
