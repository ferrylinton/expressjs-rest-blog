require('module-alias/register');

const { check } = require('express-validator');
const { Post } = require('@models/index');

var create = [
  check('code', 'code is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 100 }).withMessage('chars length min 3 and max 100')
    .bail().custom(isCodeExist),
  check('title', 'title is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 255 }).withMessage('chars length min 3 and max 255')
    .bail().custom(isTitleExist),
  check('description', 'description is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 500 }).withMessage('chars length min 3 and max 500'),
  check('file')
    .custom(checkFile).withMessage('markdown file is required')
]

var update = [
  check('code')
    .optional()
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 100 }).withMessage('chars length min 3 and max 100')
    .bail().custom(isCodeExist),
  check('title')
    .optional()
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 255 }).withMessage('chars length min 3 and max 255')
    .bail().custom(isTitleExist),
  check('description')
    .optional()
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 500 }).withMessage('chars length min 3 and max 500'),
  check('file')
    .optional()
    .custom(checkFile).withMessage('markdown file is required')
]

async function isCodeExist(code, { req }) {
  let post = await Post.findOne({
    where: { code }
  });

  if (post) {
    if (!(req.params.id && (req.params.id == post.id))) {
      return Promise.reject(`${code} is already exist`);
    }
  }

  return true;
}

async function isTitleExist(title, { req }) {
  let post = await Post.findOne({
    where: { title }
  });

  if (post) {
    if (!(req.params.id && (req.params.id == post.id))) {
      return Promise.reject(`${title} is already exist`);
    }
  }

  return true;
}

function checkFile(value, { req }) {
  if (req.file && (req.file.mimetype === 'text/markdown')) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  create,
  update
};
