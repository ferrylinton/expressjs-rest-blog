require('module-alias/register');

const { check } = require('express-validator');
const { Tag } = require('@models/index');


var validate = [
  check("name", "name is required")
    .trim()
    .escape()
    .notEmpty()
    .bail().matches(/^[A-Za-z0-9_-]+$/).withMessage("must contains alphanumeric, -, or _")
    .bail().isLength({ min: 3, max: 50 }).withMessage("chars length min 3 and max 50")
    .custom(isNameExist)
]

async function isNameExist(name, { req }) {
  let tag = await Tag.findOne({
    where: { name }
  });

  if (tag) {
    if (!(req.params.id && (req.params.id == tag.id))) {
      return Promise.reject(`${name} is already exist`);
    }
  }

  return true;
}

module.exports = {
  validate
};
