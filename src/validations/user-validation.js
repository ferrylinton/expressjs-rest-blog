require('module-alias/register');

const { check } = require('express-validator');
const { User } = require('@models/index');


var create = [
  check('username', 'username is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50')
    .custom(isUsernameExist),
  check('email', 'email is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isEmail()
    .bail().isLength({ max: 50 }).withMessage('chars length max 50')
    .custom(isEmailExist),
  check('password', 'password is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50'),
  check('passwordConfirmation', 'passwordConfirmation is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().custom((value, { req }) => value === req.body.password)
    .withMessage('passwordConfirmation field must have the same value as the password field'),
  check("role", "role is required")
    .trim()
    .escape()
    .notEmpty()
]

var update = [
  check('username')
    .optional()
    .trim()
    .escape()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50')
    .custom(isUsernameExist),
  check('email')
    .optional()
    .trim()
    .escape()
    .bail().isEmail()
    .bail().isLength({ max: 50 }).withMessage('chars length max 50')
    .custom(isEmailExist),
  check('password')
    .optional()
    .trim()
    .escape()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50')
]

var changePassword = [
  check('oldPassword', 'oldPassword is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50'),
  check('newPassword', 'newPassword is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50'),
  check('newPasswordConfirmation', 'newPasswordConfirmation is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().custom((value, { req }) => value === req.body.newPassword)
    .withMessage('newPasswordConfirmation field must have the same value as the newPassword field')
]

var authenticate = [
  check('username', 'username is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50'),
  check('password', 'password is required')
    .trim()
    .escape()
    .notEmpty()
    .bail().isLength({ min: 3, max: 50 }).withMessage('chars length min 3 and max 50')
]

async function isUsernameExist(username, { req }) {
  if (username) {
    let user = await User.findOne({
      where: { username }
    });

    if (user) {
      if (!(req.params.id && (req.params.id == user.id))) {
        return Promise.reject(`${username} is already exist`);
      }
    }
  }

  return true;
}

async function isEmailExist(email, { req }) {
  if (email) {
    let user = await User.findOne({
      where: { email }
    });

    if (user) {
      if (!(req.params.id && (req.params.id == user.id))) {
        return Promise.reject(`${email} is already exist`);
      }
    }
  }

  return true;
}

module.exports = {
  create,
  update,
  changePassword,
  authenticate
};
