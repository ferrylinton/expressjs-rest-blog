require('module-alias/register');
require('dotenv').config();

const { chmod } = require('fs');
const logger = require('@configs/logger');
const port = parseInt(process.env.PORT || '3000', 10);
const unixSocketEnable = process.env.UNIX_SOCKET_ENABLE || 'false';
const unixSocketPath = process.env.UNIX_SOCKET_PATH;

var app = require('@root/app');

app.listen((unixSocketEnable === 'true') ? unixSocketPath : port, function () {
    logger.info(`####################################################################`)
    logger.info(`NODE_ENV   : ${process.env.NODE_ENV}`);
    logger.info(`address    : ${JSON.stringify(this.address())}`);
    logger.info(`####################################################################`)

    if (unixSocketEnable === 'true') {
        chmod(unixSocketPath, 0o777, (err) => {
            if (err) throw err;
            logger.info(`The permissions for file ${unixSocketPath} have been changed!`);
        });
    }
});
